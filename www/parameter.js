// this file contains the parameter mapping logic

exports.id = function(req, res, next, id) {
    req.id = id;
    next();
};