var express   = require('express'),
    app       = express(),
    server    = require('http').createServer(app),
    fs        = require('fs'),
    database  = require('./database'),
    list      = require('./list'),
    admin     = require('./admin'),
    parameter = require('./parameter'),
    config    = require('./config');

var auth = express.basicAuth(config.username, config.password);

// use gzip
app.use(express.compress());

// serve static content from public directory
app.use(express.static(__dirname + '/public'), { maxAge: config.cacheTime });
app.use(express.bodyParser());

// no spaces in json responses
app.set('json spaces', 0);

// parameter mapping
app.param('id', parameter.id);

// routing

// picture viewing and upload
app.get('/list', list.list);
app.get('/list-approved', list.list_approved);
app.get('/list-hidden', list.list_hidden);
app.get('/picture/:id', list.picture);
app.post('/upload', database.savePicture);

// management
app.get('/admin', auth, admin.admin);
app.get('/admin/mode', auth, admin.getMode);
app.put('/admin/mode/toggle', auth, admin.toggleMode);
app.put('/admin/picture/:id/hidden', auth, admin.hidePicture);
app.put('/admin/picture/:id/visible', auth, admin.showPicture);
app.put('/admin/export', auth, admin.exportImages);

// Listen for incoming requests on port 80
server.listen(80);
