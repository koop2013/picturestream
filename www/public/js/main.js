var x;
var doUpload;


$(function() {
    (function(a){(jQuery.browser=jQuery.browser||{}).mobile=/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

    var moderate = false;
    var imageUrl = "/picture/";
    var galleryColumnCount = 3; // (>0)
    var spinner;
    var loadedElements;
    var totalElements;
    var elementsToLoad = 0;

    // development
    var updateInterval = 3000;
    var minSpin = 300;
    var showTick = 3000;

    var legacy = jQuery.browser.mobile;
    var whiteList = [
        "WebKit/537",
	"WebKit/536",
	"WebKit/535",
	"Trident/7.0",
	"Firefox/25.0",
	"Firefox/24.0",
	"Firefox/23.0",
	"Firefox/22.0",
	"Firefox/26.0"
    ];

    console.log('agent: ' + navigator.userAgent);

    for (var i = 0; i < whiteList.length; i++) {
        if (navigator.userAgent.indexOf(whiteList[i]) != -1) {
            legacy = false;
        }
    }
    
    if (legacy) {
        $("#upload_test").hide()
        $("#upload_legacy").show()
    } else {
        $("#upload_legacy").hide()
        $("#upload_test").show()
    }

    // production
    // var updateInterval = 10000;

    var initPictureStream = function() {
        var opts = {
          lines: 15, // The number of lines to draw
          length: 16, // The length of each line
          width: 6, // The line thickness
          radius: 35, // The radius of the inner circle
          corners: 1, // Corner roundness (0..1)
          rotate: 48, // The rotation offset
          direction: 1, // 1: clockwise, -1: counterclockwise
          color: '#FFF', // #rgb or #rrggbb or array of colors
          speed: 0.9, // Rounds per second
          trail: 62, // Afterglow percentage
          shadow: false, // Whether to render a shadow
          hwaccel: false, // Whether to use hardware acceleration
          className: 'spinner', // The CSS class to assign to the spinner
          zIndex: 2e9, // The z-index (defaults to 2000000000)
          top: $(window).height()/2-100+'px', // Top position relative to parent in px
          left: 'auto' // Left position relative to parent in px
        };
        spinner = new Spinner(opts).spin($('#content')[0]);
        $( ".spinner" ).append( "<p class=\"spinnerstatus\"></p>" );

        moderate = $('body').attr('data-moderate');
        console.log(moderate);
        initGallery();
        initPictureStreamReload();
        updatePictureStream();
    };

    doUpload = function(inp, btn) {
        var l = Ladda.create(btn[0]);
        var startT = new Date().getTime();
        l.start();
         // this = btn;
        $(inp).closest('form').ajaxSubmit({success: function() {
            toSleep = minSpin - (new Date().getTime() - startT);
            if (toSleep < 0) {
                toSleep = 0;
            }
            setTimeout(function() {

                $("#uploadIcon").removeClass("fa-upload").addClass("fa-check");
                setTimeout(function() {
                    $("#uploadIcon").removeClass("fa-check").addClass("fa-upload");
                }, showTick);
                l.stop();
            }, toSleep);
        }}).get(0).reset();
    }

    var initPictureStreamReload = function() {
        setTimeout(updatePictureStream, updateInterval);
    };

    var updatePictureStream = function() {
        console.log("update picture stream");

        var url = $('body').attr('data-images-url');
        var url_del = $('body').attr('data-images-url-hidden');

        if(!moderate) {
            $.getJSON(url_del, function(data) {
                $.each(data, function(key, imageData) {
                    Stream.remove(Stream.get(imageData.id));
                    StreamNot.add(new StreamItem({
                        id: imageData.id,
                        src: imageUrl + imageData.id,
                        date: imageUrl.date,
                        visible: imageData.visible
                    }));
                });
            });
        }

        $.getJSON(url, function(data) {
            totalElements = data.length;
            loadedElements = 0;
            $.each(data, function(key, imageData) {
                if(!StreamNot.contains(imageData.id)) {
                    var imageSrc = imageUrl + imageData.id;
                    Stream.add(new StreamItem({
                        id: imageData.id,
                        src: imageSrc,
                        date: imageUrl.date,
                        visible: imageData.visible
                    }));
                }
            });
        });

        setTimeout(updatePictureStream, updateInterval);
    };

    var initGallery = function() {
        var resizeColumn = function() {
            console.log($(window).width());
            $(".item").css("width", $(window).width()/galleryColumnCount-10);
        };
    }

    var LoadingStatusModel = Backbone.Model.extend({
        defaults: function() {
            return {
                count: 0
            };
        },

        increment: function() {
            this.set('count', this.get('count') + 1);
        },

        decrement: function() {
            this.set('count', this.get('count') - 1);
        }
    });

    var loadingStatus = new LoadingStatusModel();

    var LoadingStatusView = Backbone.View.extend({
        el: $(".loadingstatus"),

        initialize: function() {
            this.model.bind('change', this.render);
        },

        render: function() {
            var count = loadingStatus.get('count');
            if(count > 0) {
                $(".loadingstatus").html("loading "+count+" pictures...").fadeIn('slow');
            } else {
                $(".loadingstatus").html("images loaded").fadeOut('slow');
            }
        },
    });

    new LoadingStatusView({model: loadingStatus});

    var StreamItem = Backbone.Model.extend({
        defaults: function() {
            return {
                id: 0,
                src: "",
                date: "",
                visible: false
            };
        },

        toggle: function() {
            if(moderate) {
                this.set("visible", !this.get("visible"))
                var visibility = this.get("visible") ? 'visible' : 'hidden' ;
                $.ajax({
                    type: "PUT",
                    url: "/admin/picture/"+this.get('id')+"/"+visibility
                }).done(function(data) {
                    console.log(data);
                });
            }
        }
    });

    var StreamList = Backbone.Collection.extend({
        model: StreamItem,
        comparator: 'date'
    });

    var Stream = new StreamList;
    x = Stream;

    // elements which are not in the stream
    var StreamNot = new StreamList;

    var StreamItemView = Backbone.View.extend({
        tagname: "div",
        template: _.template($('#stream-item-template').html()),

        initialize: function() {
            if(moderate) {
                this.listenTo(this.model, 'change', function() {
                    this.$el.find('img').toggleClass('disabled', !this.model.get('visible'));
                });
                this.$el.find('img').toggleClass('disabled', !this.model.get('visible'));
            }
        },

        render: function() {
            var templateModel = this.model.attributes
            templateModel.disabled = this.model.get('visible') ? '' : 'disabled';
            this.$el.html(this.template(templateModel));
            return this;
        },

        events: {
            "click": "toggleVisibilty"
        },

        toggleVisibilty: function( event ){
            this.model.toggle();
        }
    });

    var StreamView = Backbone.View.extend({
        el: $("#stream"),

        initialize: function() {
            this.listenTo(Stream, 'add', this.addItem);
            this.listenTo(Stream, 'remove', this.removeItem);
        },

        render: function() {
        },

        addItem: function(streamItem) {
            console.log("loading: " + streamItem.get('src'));
            var that = this;
            loadingStatus.increment();
            $.imageloader({
                    urls: [streamItem.get('src')],

                    smoothing: true,

                    onComplete: function(){

                        console.log("loaded: " + streamItem.get('src'));

                        var view = new StreamItemView({model: streamItem});
                        var el = view.render().el;
                        view.$el.css({ opacity: 0.0 });
                        that.$el.prepend(el);
                        that.$el.masonry('prepended', el);
                        that.$el.masonry('layout');
                        console.log("added image: " + streamItem.id);
                        window.setTimeout(function() {
                            $('.spinner').fadeOut('slow');
                            streamView.$el.masonry('layout');
                        }, 1000);
                        window.setTimeout(function() {
                            spinner.stop();
                            $(window).trigger("resize");
                            view.$el.fadeTo( 1000 , 1.0);
                        }, 1500);

                        loadingStatus.decrement();
                    },

                    onError: function(err){
                        console.log(err);
                    }
                });
        },

        removeItem: function(streamItem) {
            var el = $("#"+streamItem.id);
            this.$el.masonry('remove', el);
            this.$el.masonry('layout');
            console.log("removed image!");
        },
    });

    var streamView = new StreamView();

    initPictureStream();

    // bad fixes for layouting lib .. :/
    window.setTimeout(function() {
        streamView.$el.masonry('layout');
    }, 1000);
    window.setTimeout(function() {
        $(window).trigger("resize");
    }, 1500);
});
