$(function() {
    var moderate = $('body').attr('data-moderate');

    if(moderate) {
        var StreamModeModel = Backbone.Model.extend({
            defaults: function() {
                return {
                    defaultVisible: false
                };
            },
            initialize: function() {
                var that = this;
                $.get("/admin/mode").done(function(data){
                    console.log("mode: " + data);
                    that.set('defaultVisible', data);
                });
            },
            toggleMode: function(newMode) {
                var that = this;
                $.ajax({
                    type: "PUT",
                    url: "/admin/mode/toggle"
                }).done(function(data) {
                    console.log("mode: " + data);
                    that.set('defaultVisible', data);
                });
            }
        });
        var StreamMode = new StreamModeModel();

        var StreamModeView = Backbone.View.extend({
            el: $("#streammode"),
            initialize: function() {
                this.model.on('change', this.render, this);
                this.$el.bootstrapSwitch();
            },
            render: function() {
                var checked = this.model.get('defaultVisible');
                this.$el.bootstrapSwitch('setState', checked, true);
                return this;
            },
            events: {
                'click':'clicked',
                'switch-change':'clicked',
            },
            clicked : function(e) {
                this.model.toggleMode();
            }
        });
        new StreamModeView({model: StreamMode});
    }
});