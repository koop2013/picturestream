var database = require('./database'),
    fs       = require('fs'),
    config   = require('./config');

var IMAGE = database.connection;

// administration stuff
exports.admin = function(req, res) {
    try {
        var path = 'moderate.html';
        fs.readFile(path, function(err, data) {
           if(err) throw err;
           res.setHeader("Content-Type", "text/html");
           res.send(data);
       });
    } catch(err) {
        console.log(err);
        res.send('Error reading moderate.html');
    }
};

exports.getMode = function(req, res) {
    res.send(config.visible);
};

exports.toggleMode = function(req, res) {
    if(config.visible) {
        config.visible = false;
    } else {
        config.visible = true;
    }
    res.send(config.visible);
};

exports.hidePicture = function(req, res) {
    displayPicture(req.id, false);
    res.send(req.id + ' set to hidden');
};

exports.showPicture = function(req, res) {
    displayPicture(req.id, true);
    res.send(req.id + ' set to visible');
};

function displayPicture(id, visible) {
    console.log(id);

    IMAGE.findById(id, function(err, result) {
        result.img.visible = visible;
        result.save();
    });
};

exports.exportImages = function(req, res) {
    IMAGE.find({ }, function(err, docs) {
	if(err) throw err;
	docs.forEach(function(value) {
	    fs.writeFile('/tmp/all/img-' + value.img.date + '.jpg', value.img.data, function(err) {
		if(err) throw err;	
	    });
	});
	res.send('started export, check your /tmp folder');
    });
}
