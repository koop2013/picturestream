var database = require('./database');
var config = require('./config');
var IMAGE = database.connection;

function findByJSON(filter, req, res) {
    try {
        IMAGE.find(filter, function(err, docs) {
            if(err) throw err;
            var list = [];
            docs.forEach(function(value) {
                list.push({
                    'id' : value._id,
                    'date' : value.img.date,
                    'visible' : value.img.visible
                });
            });
            res.send(list);
        });
    } catch(err) {
        console.log('there was an error when listing images');
        console.log(err);
    }
}

exports.list = function(req, res) {
    findByJSON({}, req, res);
};

exports.list_approved = function(req, res) {
    IMAGE.find({ 'img.visible' : true }).sort({_id:-1}).limit(config.number_of_items).exec(function(err, docs) {
        var list = [];
	docs.reverse();
        docs.forEach(function(value) {
            list.push({
                'id' : value._id,
                'date' : value.img.date,
                'visible' : value.img.visible
            });
        });
        res.send(list);
    });
};

exports.list_hidden = function(req, res) {
    findByJSON({ 'img.visible' : false }, req, res);
};

exports.picture = function(req, res) {
    // get image by id from mongo
    try {
        IMAGE.findById(req.id, function(err, result) {
            if(err) throw err;
            if(result !== null) {
                res.contentType(result.img.contentType);
                res.send(result.img.data);
            } else {
                res.send('image not found');
            }
        });
    } catch(err) {
        console.log('there was an error when retrieving an image');
        console.log(err);
    }
};
