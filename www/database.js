var mongoose = require('mongoose'),
    fs       = require('fs'),
    easyimg  = require('easyimage'),
    config   = require('./config');

var Schema = mongoose.Schema;
var schema = new Schema({
    img: {
        data: Buffer,
        contentType: String,
        date: Date,
        visible: Boolean,
    }
});

mongoose.connect('localhost', 'test_image');

var IMAGE = mongoose.model('image', schema);
exports.connection = IMAGE;

mongoose.connection.on('open', function() {
    console.log('connected to mongodb');

    // clear database
    if (config.clear_database_on_startup) {
        try {
            console.log('clearing database');
            IMAGE.remove(function (err) {
                if (err) throw err;
            });
        } catch(err) {
            console.log('error connecting to database');
            console.log(err);
        }
    }

});


exports.savePicture = function(req, res) {
    // this will be used to save pictures to mongodb
    var file = req.files.displayImage;
    type = file.headers['content-type'];

    // resize and crop picture
    try {
        easyimg.info(file.path, function(err, fileinfo, stderr) {
            resultWidth = fileinfo.width > fileinfo.height ? 900 : 600;
            resultHeight = fileinfo.width > fileinfo.height ? 600 : 900;

            if(fileinfo.type === 'JPEG' ||
                fileinfo.type === 'PNG') {

                easyimg.rescrop({
                    src: file.path,
                    dst: file.path + '-processed',
                    width: resultWidth,
                    height: resultHeight,
                    quality: 80
                },
                function(err, image) {
                    if(err) throw err;
                    console.log(image);

                    // read file from temp folder and save to mongodb
                    fs.readFile(file.path + '-processed', function(err, data) {
                        var image = new IMAGE();
                        image.img.contentType = type;
                        image.img.date = new Date();
                        image.img.data = data;
                        image.img.visible = config.visible;

                        image.save(function(err, image) {
                            if(err) throw err;
                            console.log('saved new image to mongo');
                            console.log('deleting tmp files');
                            fs.unlink(file.path, function(err) {
                                if (err) throw err;
                                fs.unlink(file.path + '-processed', function(err) {
                                    if (err) throw err;
                                });
                            });
                        });
                    });
                });
            }
        });
        res.redirect('/');
    } catch(err) {
        // res.send('there was an error when uploading the image');
        res.redirect('/');
        console.log('image upload error');
        console.log(err);
    }
};
