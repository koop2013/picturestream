# Kooperatives Arbeiten
Winter Term 2013
_Vienna University of Technology_


# Bilder vom Event
Hier könnt ihr die Bilder vom Koop-Event herunterladen:

 * [Angezeigte Bilder](https://bitbucket.org/koop2013/picturestream/downloads/koop-pictures.zip) _<-- will man normalerweise_
 * [Alle Bilder](https://bitbucket.org/koop2013/picturestream/downloads/koop-all-pictures.zip)
 * [Nur geblockte Bilder ;-)](https://bitbucket.org/koop2013/picturestream/downloads/koop-hidden-pictures.zip)

__Wir hoffen ihr hattet keine Probleme mit unserem Tool und viel Spaß beim Event :-)__

Falls ihr das Ganze einmal selbst betreiben wollt, fragt uns einfach falls ihr wo Probleme habt.

_Für die Abenteuerlichen hier noch ein [Mongo-Dump](https://bitbucket.org/koop2013/picturestream/downloads/mongo_dump.tgz)._

## Team
 * Thomas Rieder
 * Manuel Geier
 * Florian Mayer
 * Jonas Windhager

## Idea
Wir werden eine Live-Photo-Wall machen. Es wird einen Beamer geben, auf dem die Anwendung/Live-Wall präsentiert wird. Jeder Teilnehmer hat die Möglichkeit, Fotos mit seinem Smartphone auf diese Wall zu laden.

## Technologies
 * Ubuntu Server
 * Vagrant
 * Node.JS
 * MongoDB
 * express.js
 * mongoose
 * HTML, CSS, JS (_duh_) and a crapton of other $something.js

## Contact
 * [Thomas](http://rieder.io)
 * [Manuel](http://manuelgeier.com)